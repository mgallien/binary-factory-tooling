// Ask for parameters we will need later on
def buildParameters = input(
	message: 'Which version of Krita is being built?',
	ok: 'Begin Build',
	parameters: [
		choice(choices: "stable\nunstable\n", description: '', name: 'Release'), 
		string(defaultValue: '', description: '', name: 'Version', trim: true)
	]
)

// Request a node to be allocated to us
node( "Appimage1604" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Now we download the release tarball, unpack it and rename the directory to something more convenient to use everywhere else
			sh """
				wget "https://origin.download.kde.org/${buildParameters['Release']}/krita/${buildParameters['Version']}/krita-${buildParameters['Version']}.tar.gz"

				tar -xf "$WORKSPACE/krita-${buildParameters['Version']}.tar.gz"

				mv krita-${buildParameters['Version']} krita
			"""
		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-appimage-deps.tar, gmic_krita_qt-x86_64.appimage', projectName: 'Krita_Nightly_Appimage_Dependency_Build'

			// Now we unpack them
			sh """
				mkdir -p $HOME/appimage-workspace/
				cd $HOME/appimage-workspace/
				tar -xf $WORKSPACE/krita-appimage-deps.tar
			"""
		}

		// Let's build Krita that we have everything we need
		stage('Building Krita') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-krita.sh $HOME/appimage-workspace/ $WORKSPACE/krita/
			"""
		}

		// Now we can generate the actual Appimages!
		stage('Generating Krita Appimage') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-image.sh $HOME/appimage-workspace/ $WORKSPACE/krita/
				mv $HOME/appimage-workspace/*.appimage $WORKSPACE/
			"""
		}

		// Finally we capture the appimages for distribution to users
		stage('Capturing Appimages') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.appimage', onlyIfSuccessful: true
		}
	}
}
}
