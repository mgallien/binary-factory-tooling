// Request a node to be allocated to us
node( "macOS" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/krita']]
			]

		}

		// Now we can build the Dependencies for Krita's build
		stage('Building Dependencies') {
			// This is relatively straight forward
			// We use some environment variables to tell the Krita scripts where we want them to put things
			// Then we invoke them!
			sh """
				export PATH=\$HOME/Craft/Krita/macos-64-clang/bin/:\$HOME/Craft/Krita/macos-64-clang/dev-utils/bin/:\$PATH
				export BUILDROOT=\$HOME/KritaBuild/

				[[ -d \$BUILDROOT ]] || mkdir \$BUILDROOT
				[[ -s \$BUILDROOT/krita ]] || ln -s \$WORKSPACE/krita \$BUILDROOT/krita

				bash krita/packaging/macos/osxbuild.sh builddeps

				cd \$BUILDROOT/depbuild/ext_qt/ext_qt-prefix/src/ext_qt/qttools/src
				make sub-macdeployqt-all
				make sub-macdeployqt-install_subtargets
				make install
			"""
		}

		// Now we capture them for use
		stage('Capturing Dependencies') {
			// First we tar all of the dependencies up...
			sh """
				cd \$HOME/KritaBuild/
				tar -cf \$WORKSPACE/krita-macos-deps.tar i/
				
				rm -rf \$HOME/KritaBuild/
			"""

			// Then we ask Jenkins to capture the tar file as an artifact
			archiveArtifacts artifacts: 'krita-macos-deps.tar', onlyIfSuccessful: true
		}
	}
}
}
